# FrivyCat-Inc-Mod
![](logo.png)
<img alt="Discord" src="https://img.shields.io/discord/762144607261163531?label=Discord&logo=discord&logoColor=lime&style=flat-square">
<img alt="GitHub contributors" src="https://img.shields.io/github/contributors/ChaosDogG/FrivyCat-Inc-Mod?logo=github&style=flat-square">
<img alt="GitHub last commit" src="https://img.shields.io/github/last-commit/ChaosDogG/FrivyCat-Inc-Mod?logo=github&style=flat-square">
<img alt="GitHub language count" src="https://img.shields.io/github/languages/count/chaosdogg/frivycat-inc-mod?logo=github&style=flat-square">
<img alt="GitHub" src="https://img.shields.io/github/license/chaosdogg/frivycat-inc-mod?logo=github&style=flat-square">
<img alt="Custom badge" src="https://img.shields.io/endpoint?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F848454691805069312%2F866417009566285864%2FMC_Version_Badge_2.json">
<img alt="Twitch Status" src="https://img.shields.io/twitch/status/chaosdog1?style=social">
<img src="https://badges.crowdin.net/frivycat-inc-mod-translations/localized.svg">

This mod is designed to be based off of my datapack of the same name, FrivyCat Inc Datapack. However, this is meant as a successor so any future additions will most likely happen here before the DP will receive them.

<img alt="Custom badge" src="https://img.shields.io/endpoint?url=https%3A%2F%2Fcdn.discordapp.com%2Fattachments%2F848454691805069312%2F866428895224922122%2Ftolololololol.json">
